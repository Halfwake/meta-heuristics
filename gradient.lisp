(defun derivative (foo &optional (epsilon 0.001))
  (lambda (x)
    (/ (- (funcall foo (+ x epsilon))
          (funcall foo x))
       epsilon)))

(defun gradient-descent-1d (foo init-val &optional (time-left 1000) (alpha 0.001))
  (if (plusp time-left)
      (let ((time-at-entry (get-internal-real-time)))
        
        (gradient-descent-1d foo
                             (- init-val
                                (* alpha
                                   (funcall (derivative foo) init-val)))
                             (- time-left
                                (- (get-internal-real-time) time-at-entry))
                             alpha)
        init-val)))

(defun return-greater-quality (quality a b)
  (if (> (funcall quality a) (funcall quality b))
      a
      b))

(defun hill-climb (init-val copy tweak quality &optional (time-left 1000))
  (if (plusp time-left)
      (let ((time-at-entry (get-internal-real-time))
            (tweaked-copy (funcall tweak (funcall copy init-val))))
        (hill-climb (return-greater-quality quality tweaked-copy init-val)
                    copy
                    tweak
                    quality
                    (- time-left
                       (- (get-internal-real-time) time-at-entry))))
      init-val))

(defun steepest-ascent-hill-climb (guess copy tweak quality &optional (tweak-number 10) (time-left 1000))
  (if (plusp time-left)
      (let ((time-at-entry (get-internal-real-time))
            (best-tweaked-copy (reduce (alexandria:curry #'return-greater-quality quality)
                                       (mapcar (alexandria:compose tweak copy)
                                               (make-list tweak-number :initial-element guess)))))
        (steepest-ascent-hill-climb (return-greater-quality quality best-tweaked-copy guess)
                                    copy
                                    tweak
                                    quality
                                    tweak-number
                                    (- time-left
                                       (- (get-internal-real-time) time-at-entry))))
      guess))

(defun maybe ()
    (zerop (random 2)))

(steepest-ascent-hill-climb 1
                            #'identity
                            #'(lambda (val)
                                (if (maybe)
                                    (1+ val)
                                    (1- val)))
                            #'(lambda (val)
                                (let ((difference (- val 100)))
                                  (if (zerop difference)
                                      2
                                      (/ 1
                                         (abs (- val 100)))))))

(hill-climb 1
            #'identity
            #'(lambda (val)
                (if (maybe)
                    (1+ val)
                    (1- val)))
            #'(lambda (val)
                (let ((difference (- val 100)))
                  (if (zerop difference)
                      2
                      (/ 1
                         (abs (- val 100)))))))

(defun random-range (min max)
  (let ((distance (abs (- min max))))
    (+ min (random distance))))

(defun random-search (guess quality randomizer &optional (time-left 1000))
  (if (plusp time-left)
      (let ((time-at-entry (get-internal-real-time)))
        (random-search (return-greater-quality quality
                                               (funcall randomizer)
                                               guess)
                       quality
                       randomizer
                       (- time-left
                          (- (get-internal-real-time) time-at-entry))))
      guess))
                       

(random-search 10
               #'(lambda (val)
                   (let ((difference (- val 100)))
                     (if (zerop difference)
                         2
                         (/ 1
                            (abs (- val 100))))))
               #'(lambda () (random-range 0 300)))

(defun hill-climb-with-random-restarts (guess copy tweak quality make-time &optional (time-left 1000))
  (if (plusp time-left)
      (let ((time-at-entry (get-internal-real-time)))
        (hill-climb-with-random-restarts (return-greater-quality quality
                                                                 (hill-climb guess
                                                                             copy
                                                                             tweak
                                                                             quality
                                                                             (funcall make-time))
                                                                 guess)
                                         copy
                                         tweak
                                         quality
                                         make-time
                                         (- time-left
                                            (- (get-internal-real-time) time-at-entry))))
      guess))

(hill-climb-with-random-restarts 10
                                 #'identity
                                 #'(lambda (val)
                                     (if (maybe)
                                         (1+ val)
                                         (1- val)))
                                 #'(lambda (val)
                                     (let ((difference (- val 100)))
                                       (if (zerop difference)
                                           2
                                           (/ 1
                                              (abs (- val 100))))))
                                 (constantly 100))

(defun bounded-uniform-convolution-adjust (val probability half-range min max)
  (if (>= probability (random 1.0))
      (let ((new-val (+ val
                        (random-range (- half-range) half-range))))
        (if (and (< min new-val) (< new-val max))
            new-val
            (bounded-uniform-convolution-adjust val probability half-range min max)))
      val))

(defun bounded-uniform-convolution (val probability half-range min max)
  (mapcar (alexandria:rcurry #'bounded-uniform-convolution-adjust 
                             probability
                             half-range
                             min
                             max)
          val))
      
(bounded-uniform-convolution '(1 2 3 4 5)
                             0.5
                             0.1
                             0
                             6)

(defun gaussian-distribution-random (mean variance)
  (let ((x (random-range -1.0 1.0))
        (y (random-range -1.0 1.0)))
    (flet ((valid-wp (w)
             (and (< 0 w) (< w 1)))
           (make-guess (w x-or-y)
             (+ mean
                (* x-or-y
                   variance
                   (sqrt (* -2
                            (/ (log w) w))))))) ;; TODO Check if log is ln
      (let ((w (+ (* x x)
                  (* y y))))
        (if (valid-wp w)
            (let ((g (make-guess w x))
                  (h (make-guess w y))) ;; Either g or h will work. It doesn't matter.
              g)
            (gaussian-distribution-random mean variance))))))
            
    

(defun gaussian-convolution-adjust (val probability variance min max)
  (if (>= probability (random 1.0))
      (let ((new-val (+ val (gaussian-distribution-random 0 ; TODO mean value
                                                          variance))))
        (if (and (< min new-val) (< new-val max))
            new-val
            (gaussian-convolution-adjust val probability variance min max)))
      val))


(defun gaussian-convolution (val probability variance min max)
  (mapcar (alexandria:rcurry #'gaussian-convolution-adjust
                             probability
                             variance
                             min
                             max)
          val))

(defun simulate-annealing (guess temperature decrease-temperature copy tweak quality &optional (time-left 1000) (best guess))
  (if (and (plusp time-left) (plusp temperature))
      (let* ((time-at-entry (get-internal-real-time))
             (possible-guess (funcall tweak (funcall copy guess)))
             (possible-guess-quality (funcall quality possible-guess))
             (guess-quality (funcall quality guess))
             (best-quality (funcall quality best)))
        (simulate-annealing (if (or (> possible-guess-quality guess-quality)
                                    (< (random 1.0)
                                       (expt (exp 1)
                                             (/ (- possible-guess-quality guess-quality)
                                                temperature))))
                                possible-guess
                                guess)
                            (funcall decrease-temperature temperature)
                            decrease-temperature
                            copy
                            tweak
                            quality
                            (- time-left
                               (- (get-internal-real-time) time-at-entry))
                            (if (> possible-guess-quality best-quality)
                                possible-guess
                                best)))
      best))

(simulate-annealing 5.0
                    0.1
                    #'(lambda (x)
                        x)
                    #'identity
                    #'(lambda (val)
                        (if (maybe)
                            (+ 10 val)
                            (+ 10 val)))
                    #'(lambda (val)
                        (let ((difference (- val 100)))
                          (if (zerop difference)
                              2
                              (/ 1
                                 (abs (- val 100)))))))

(defun tabu-search-gradient-best (guess gradient-size memberp copy tweak quality)
  (reduce (lambda (a b)
            (if (and (not (funcall memberp b))
                     (or (> (funcall quality a)
                            (funcall quality b))
                         (funcall memberp a)))
                b
                a))
          (mapcar (alexandria:compose tweak copy)
                  (make-list gradient-size :initial-element guess))))
  

(defun tabu-search (guess tabu-length gradient-size equalp copy tweak quality &optional (time-left 1000) (tabu-list '()))
  (if (plusp time-left)
      (let ((time-at-entry (get-internal-real-time)))
        (tabu-search (flet ((memberp (item)
                              (member item tabu-list :test equalp)))
                       (let ((possible-guess (tabu-search-gradient-best guess
                                                                        gradient-size
                                                                        #'memberp
                                                                        copy
                                                                        tweak
                                                                        quality)))
                         (if (or (memberp possible-guess) (> (funcall quality guess)
                                                             (funcall quality possible-guess)))
                             guess
                             possible-guess)))
                     tabu-length
                     gradient-size
                     equalp
                     copy
                     tweak
                     quality
                     (- time-left
                        (- (get-internal-real-time) time-at-entry)) ;; TODO let and if
                     (if (>= (length tabu-list) tabu-length)
                         (cons guess (butlast tabu-list))
                         (cons guess tabu-list))))
      guess))

(tabu-search  5.0
              5
              5
              #'=
              #'identity
              #'(lambda (val)
                  (if (maybe)
                      (+ 15 val)
                      (+ 15 val)))
              #'(lambda (val)
                  (let ((difference (- val 100)))
                    (if (zerop difference)
                        2
                        (/ 1
                           (abs (- val 100)))))))
        
                    
                                   
                               
                            
                                
                                     
                                     
        
                     


